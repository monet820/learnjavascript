/**
 * 
 */

// DOM elements from HTML.
const domPay = document.getElementById("bankAmount");
const domButtonWork = document.getElementById("button-work");
const domButtonTransferWork = document.getElementById("button-transfer-work");
const domButtonLoan = document.getElementById("button-loan");
const domButtonBuyLaptop = document.getElementById("button-buy");

// Dropdown list for laptops
const domLaptops = document.getElementById("laptops");

// elements supposed to be written over.
const domWorkAmount = document.getElementById("workAmount");
const domBalanceAmount = document.getElementById("bankAmount");
const domLoanAmount = document.getElementById("loanAmount");
const domFeatureLaptops = document.getElementById("featuresLaptops");

// elements used for css properties.
const domLoanAmountDiv = document.getElementById("loanAmountDiv");

// Image elements
const domImageLaptop = document.getElementById("image-laptop");
const domLaptopDisplayText = document.getElementById("laptopDisplayText");
const domLaptopSelectedName = document.getElementById("laptopSelectedName");

// Global variables, to be used.
let workAmount = 0;
let balanceAmount = 0;
let loanAmount = 0;
let strUserJson;


// Bank button onclick, updates loan amount.
domButtonLoan.onclick = function () {

    if (balanceAmount == 0) {
        alert("Try working some more, you can only loan twice your current balance.")
    }
    else {
        if (loanAmount > 0) {
            alert("You can not have two loans.");
        }
        else {
            let localLoanAmount = window.prompt("Please specify amount to loan." + "You can maximum loan: " + 2 * balanceAmount, "0");

            if (localLoanAmount > 2 * (balanceAmount)) {
                alert("You can only loan twice the amount of your balance.")
            }
            else {
                domLoanAmountDiv.style.visibility = "visible";
                console.log(loanAmount);
                loanAmount = localLoanAmount;
                domLoanAmount.innerHTML = loanAmount;
            }
        }
    }

}

// Work button onclick, updates work amount.
domButtonWork.onclick = function () {
    try {
        workAmount += 100;
        domWorkAmount.innerHTML = workAmount;
        console.log(workAmount);

    } catch (error) {
        console.log(error);
    }
}

// Transfer button onclick, updates balance and resets work amount.
domButtonTransferWork.onclick = function () {
    try {
        let transferComplete = false;
        while (!transferComplete) {
            balanceAmount += workAmount;
            domBalanceAmount.innerHTML = balanceAmount;
            domWorkAmount.innerHTML = 0;
            transferComplete = true;
            workAmount = 0;
        }
    } catch (error) {
        console.log(error);
    }
}

domLaptops.onchange = function () {
    let strUser = domLaptops.options[domLaptops.selectedIndex].value;
    strUserJson = JSON.parse(strUser);

    domFeatureLaptops.innerText = strUserJson.feature;
    domImageLaptop.src = strUserJson.image;

    domLaptopDisplayText.innerText = "Name: " + strUserJson.brand + "\n Price: " + strUserJson.price + "\n Description: " + strUserJson.description;
    domLaptopSelectedName.innerText = strUserJson.brand;
}


domButtonBuyLaptop.onclick = function () {
    try {
        if (strUserJson.price > balanceAmount + loanAmount) {
            alert("Work more, to afford this laptop");
        } else if (strUserJson.price <= loanAmount) {
            loanAmount -= strUserJson.price;
            domLoanAmount.innerHTML = loanAmount;
            alert("You bought " + strUserJson.brand);
        } else if (strUserJson.price <= balanceAmount + loanAmount) {
            let restPay = strUserJson.price;

            restPay = restPay - loanAmount;

            loanAmount = 0;
            domLoanAmount.innerHTML = 0;

            domBalanceAmount.innerHTML = balanceAmount - restPay;
            balanceAmount = balanceAmount - restPay;
            alert("You bought " + strUserJson.brand);
        }

    }
    catch (error) {
        alert(error);
    }
}


